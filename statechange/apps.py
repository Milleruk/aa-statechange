from django.apps import AppConfig


class StatechangeConfig(AppConfig):
    name = 'statechange'
    label = 'statechange'
