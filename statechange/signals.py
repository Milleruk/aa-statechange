import logging

from django.dispatch import receiver

from allianceauth.authentication.signals import state_changed
from allianceauth.authentication.models import UserProfile

logger = logging.getLogger(__name__)


@receiver(state_changed, sender=UserProfile)
def member_state_changed(sender, user, state, **kwargs):
    logger.debug("member_state_changed HIT")
    logger.debug("Received state_changed from %s to state %s" % (user, state))
    groups = [group for group in user.groups.all()]
    for group in groups:
        logger.debug("Removing group %s from user %s" % (group, user))
        user.groups.remove(group)
